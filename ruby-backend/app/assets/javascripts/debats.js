$(document).ready(function () {
    $(".Datepicker").datepicker({
        format: "dd/mm/yyyy",
        startView: 1,
        todayBtn: "linked",
        language: "fr",
        autoclose: true,
        todayHighlight: true
    });
});