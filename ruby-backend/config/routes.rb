Rails.application.routes.draw do

  get 'home/home'

  root                  'home#home'

  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
  resources :users,               path: 'utilisateurs'
  resources :account_activations, path: 'activation',       only: [:edit]
  resources :password_resets,                               only: [:new, :create, :edit, :update]

  resources :subjects,            path: 's', param: :subject_id do
    member do
      resources :positions,       as: 'subjects_positions', only: [:index]
      resources :statements,      as: 'subjects_statements'
    end
  end
  resources :public_figures,      path:'p', param: :public_figure_id do
    member do
      resources :statements,      as: 'public_figures_statements'
    end
  end
  resources :positions,                                     only: [:create, :update, :destroy]
  resources :statements,                                    only: [:new, :create, :update, :destroy]
  resources :arguments,                                     only: [:create, :update, :destroy]

  # AUTOCOMPLETE
  get 'autocomplete/autocomplete_subject_title'
  get 'autocomplete/autocomplete_position_title'
  get 'autocomplete/autocomplete_public_figure_name'

  get 'a-propos',             to: 'static_pages#a_propos',      as: 'a_propos'
  get 'contact',              to: 'static_pages#contact',       as: 'contact'
  get 'mode_demploi',         to: 'static_pages#mode_demploi',  as: 'mode_demploi'
  get 'error',                to: 'static_pages#error',  as: 'error'

  # HOMEPAGE
  get 'home'   => 'home#home'


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
